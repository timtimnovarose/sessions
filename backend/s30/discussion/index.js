console.log('ES6 Updates')

// Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
/*
	- allows us to write strings without using concatenation operator (+)
	- greatly helps us with code readability
*/
let name = "Ken";

// using concatenation
let message = 'Hello ' + name + '! Welcome to Programming';
console.log('Message without template literals: ' + message);

// using template literals
// backticks (``) and ${} for including javascript expressions
message = `Hello ${name}! Welcome to Programming`;
console.log(`Message with template literals: ${message}`);

// creates multi-line using template literals
const anotherMessage = `
${name} attended a Math Competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// Array Destructuring
const fullName = ["Juan", "Dela", "Cruz"];

// using array indeces
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// using array destructuring
/*
const firstName = fullName[0];
const middleName = fullName[1];
const lastName = fullName[2];
*/
const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// Object Destructuring
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: "Cruz"
}

// using dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// using object destructuring
const {givenName, familyName, maidenName} = person;

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`);

// using object destructuring in functions
function getFullName({givenName, maidenName, familyName}) {

	console.log(`${givenName} ${maidenName} ${familyName}`)
};

getFullName(person);

// Arrow Functions
const hello = () => {
	console.log('Hello Madlang People!')
}

hello();

// Traditional Functions
/*function printFullName (fName, mName, lName) {
	console.log(fName + ' ' + mName + ' ' + lName);
}

printFullName('John', 'D', 'Smith');*/

// Arrow Function with Template Literals
const printFullName = (fNAme, mName, lName) => {
	console.log(`${fNAme} ${mName} ${lName}`)
}

printFullName('Jane', 'D', 'Smith');

// Arrow Functions with Loops
const students = ['John', 'Jane', 'Judy']

// Traditional Function
students.forEach(function(student) {
	console.log(`${student} is a student.`)
})

// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statements

// Traditional Function
/*function add(x, y) {
	return x+y;     let result = x + y;
					return result; (not applicable for implicit return statement)
}

let total = add(2, 5);
console.log(total);*/

// Arrow Function
// single line arrow function (no need 'return')
const add = (x, y) => x + y;

let total = add(2, 5);
console.log(total);

// Default Argument Values (naa sa parameter)
const greet = (name = 'User') => {
	return `Good Afternoon ${name}`
}

console.log(greet()); // Good Afternoon User
console.log(greet('Judy')); // Good Afternoon Judy

// [SECTION] Class-based Object Blueprints
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instatiate an object
const fordExplorer = new Car();

// Even though the 'fordExplorer' object is const, since it is an object, you may still re-assign values to its properties.
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

// This logic applies whether you re-assign the values of each property separately or put it as arguments of the new instance of the class.
const toyotVios = new Car("Toyota", "Vios", 2028);

console.log(toyotVios);