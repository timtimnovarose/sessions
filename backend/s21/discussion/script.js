// Function Declaration and Invocation
// function printName() {
// 	console.log("My name is Nova");
// }

// printName();

// // Function Expression
// let variable_function = function() {
// 	console.log("Hello from function expression!")
// }
// variable_function();

// Scoping
let global_variable = "Call me Mr. Worldwide";

console.log(global_variable);

function showNames() {
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);

	// You CAN use global variable inside any function as long as they are declared outside of the function scope.
	console.log(global_variable);
}

// You CANNOT use locally-scoped variables outside the function they are declared in.
// console.log(funtion_variable);

showNames();

// NESTED FUNCTIONS
function parentFunction() {
	let name = "Jane";

	function childFunction() {
		let nested_name = "John";

		// this will display the output/within the same scope
		console.log(name);

		// Accessing the 'nested_name' variable within the same function it was declared in, WILL work.
		console.log(nested_name);
	}

	childFunction();

	// Accessing the 'nested_name' variable within the same function it was declared, WILL NOT work.
	// console.log(nested_name);

}

parentFunction();

// childFunction();

// BEST PRACTICES FOR FUNCTION NAMING
// function printWelcomeMessageForUser() {
// 	let first_name = prompt("Enter your first name: ");
// 	let last_name = prompt("Enter your last name: ");

// 	console.log("Hello, " + first_name + " " + last_name + "!");
// 	console.log("Welcome sa page ko!");

// }
// printWelcomeMessageForUser();


// RETURN STATEMENT
function fullName() {
	// return "Nova Timtim";
	console.log("Nova Timtim")
}

// console.log(fullName());
fullName();