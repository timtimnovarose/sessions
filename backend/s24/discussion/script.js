// [SECTION] While Loop
let count = 5;

while(count !== 0) {
	console.log("Current value of count: " + count);
	count--;
}

// [SECTION] Do-while Loop
'Number' will convert the input to number data type
let number = Number(prompt("Give me a number:"));

do {
	number += 1;
	console.log("Current value of number: " + number);

	number += 1;
} while (number < 10)

// [SECTION] For Loop
for(let count = 0; count <= 20; count++) {
	console.log("Current for loop value: " + count);
}


let my_string = "nova";

// To get the length of a string
console.log(my_string.length);

// To get a specific letter in a string
console.log(my_string[2]);

// Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
for(let index = 0; index < my_string.length; index++) {
	console.log(my_string[index]); //starts at letter 'n' and will keep printing each letter up to 'a'.
}

// MINI ACTIVITY (20 mins.)
// 1. Loop through the 'my_name' variable which has a string with your name on it.
// 2. Display each letter in the console but exclude all the vowels from it.


// let my_name = "Nova Rose Timtim";
// let vowels = "AEIOUaeiou";

// for(let index = 0; index < my_name.length; index++) {
// 	let consonants = my_name[index];
// 	if(vowels.indexOf(consonants) === -1) {
// 		console.log(consonants);
// 	}
// }


let my_name = "Nova Rose Timtim";

for(let index = 0; index < my_name.length; index++) {
	// For filtering the vowels
	if(
		my_name[index].toLowerCase() == "a" ||
		my_name[index].toLowerCase() == "e" ||
		my_name[index].toLowerCase() == "i" ||
		my_name[index].toLowerCase() == "o" ||
		my_name[index].toLowerCase() == "u"
		) {
		// If the current letter matches any of the vowels, it will just print an empty string
		// console.log("");
		continue; //If we use 'continue' keyword, it will skip the else block and reiterate the loop to check if the next letter is a vowel.
	} else {
		// Prints the current letter if it is not a vowel
		console.log(my_name[index]);
	}
}



let name_two = "rafael";

for(let index = 0; index < name_two.length; index++) {
	// console.log(name_two[index]);

	// If the current letter is 'a' then it will reiterate the loop
	if(name_two[index].toLowerCase() == "a") {
		console.log("Skipping...");
		continue;
	}

	// If the current letter is 'e' then it will break/stop the whole loop entirely
	if(name_two[index] == "e") {
		break;
	}

	console.log(name_two[index]);
}











// ------------------------------------------------------------------

// Part a - Create a variable number that will store the value of the number provided by the user via the prompt.
// let number = parseInt(prompt("Enter a number:"));

// // Part b - Create a for loop that will be initialized with the number provided by the user,
// // will stop when the value is less than or equal to 0, and will decrease by 1 every iteration.
// for (let value = number; value > 0; value--) {
//   // Part c - Create a condition that if the current value is less than or equal to 50, stop the loop.
//   if (value <= 50) {
//     break;
//   }
  
//   // Part d - Create another condition that if the current value is divisible by 10,
//   // print a message that the number is being skipped and continue to the next iteration of the loop.
//   if (value % 10 === 0) {
//     console.log(`The number is ${value}. It is divisible by 10. Skipping the number.`);
//     continue;
//   }
  
//   // Part e - Create another condition that if the current value is divisible by 5, print the number.
//   if (value % 5 === 0) {
//     console.log(value);
//   }
// }

// // Part f - Create a variable named string that will contain the string "supercalifragilisticexpialidocious".
// let string = "supercalifragilisticexpialidocious";

// // Part g - Create another variable named filteredString that will store the consonants from the string.
// let filteredString = "";

// // Part h - Create another for Loop that will iterate through the individual letters of the string based on its length.
// for (let index = 0; index < string.length; index++) {
//   let letter = string[index].toLowerCase(); // Convert the letter to lowercase for vowel comparison.
  
//   // Part i - Create an if statement that will check if the letter of the string is equal to a vowel
//   // and continue to the next iteration of the loop if it is true.
//   if (letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u") {
//     continue;
//   }
  
//   // Part j - Create an else statement that will add the letter to the second variable.
//   filteredString += string[i];
// }

// console.log(filteredString); // Output the filtered string with only consonants.
