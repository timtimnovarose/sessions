// JSON Format Example
/*{
	"city": "Pateros",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// [SECTION] JSON Arrays
/*"cities": [
	{
		"city": "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "Batangas City",
		"province": "Batangas",
		"country": "Philippines"
	},
	{
		"city": "Star City",
		"province": "Pasay",
		"country": "Philippines"
		"rides": [
			{
				"name": "Star Flyer"
			},
			{
				"name": "Gabi ng Lagim"
			}
		]
	}

]*/

// [SECTION] JSON Methods
let zuit_batches = [
	{ batchName: "303" },
	{ batchName: "271" }
]

console.log("Output before stringification:");
console.log(zuit_batches);

// After the JSON.stringify function, javascript now reads the variable as a string (equivalent to converting the array into JSON format.)
console.log("Output after stringification:");
console.log(JSON.stringify(zuit_batches));

// User details
let first_name = prompt("What is your first name?");
let last_name = prompt("What is your last name?");


// The stringify function/method converts JS object/array into JSON string/
let other_data = JSON.stringify({
	firstNAme: first_name,
	lastName: last_name
})

console.log(other_data);


// [SECTION] Convert Stringified JSON into Javascript Object/Arrays
let other_data_JSON = `[{ "firstNAme": "Nova", "lastName": "Timtim" }]`;

// The parse function/method converts the JSON string into a JS object/array
let parsed_other_data = JSON.parse(other_data_JSON);

console.log(parsed_other_data);

// Since the JSON is converted, you can now access the properties in regular javascript fashion.
console.log(parsed_other_data[0].firstNAme);